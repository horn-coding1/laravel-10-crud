<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $employees = Employee::latest()->paginate(10);

        return view('employee.index', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('employee.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $employee = new Employee();

        $employee->first_name = $request->first_name;
        $employee->last_name = $request->last_name;
        $employee->date_of_birth = $request->date_of_birth;
        $employee->gender = $request->gender;
        $employee->age = $request->age;
        $employee->salary = $request->salary;

        $employee->save();

        session()->flash('success', 'Employee created successfully.');

        return redirect()->route('employee');
    }

    /**
     * Display the specified resource.
     */
    public function clone(string $id)
    {
        $employee = Employee::findOrFail($id);

        $cloneEmployee = $employee->replicate();

        $cloneEmployee->save();

        session()->flash('success', 'Employee cloned successfully.');

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $employee = Employee::findOrFail($id);

        return view('employee.edit', compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $employee = Employee::findOrFail($id);

        $employee->first_name = $request->first_name;
        $employee->last_name = $request->last_name;
        $employee->date_of_birth = $request->date_of_birth;
        $employee->gender = $request->gender;
        $employee->age = $request->age;
        $employee->salary = $request->salary;

        $employee->save();

        session()->flash('success', 'Employee updated successfully.');

        return redirect()->route('employee');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $employee = Employee::findOrFail($id);

        $employee->delete();

        session()->flash('success', 'Employee deleted successfully.');

        return redirect()->back();
    }
}
