<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::controller(\App\Http\Controllers\EmployeeController::class)->group(function () {
    Route::get('/employee', 'index')->name('employee');
    Route::get('/employee/create', 'create')->name('employee.create');
    Route::post('/employee/store', 'store')->name('employee.store');
    Route::get('/employee/edit/{id}', 'edit')->name('employee.edit');
    Route::post('/employee/update/{id}', 'update')->name('employee.update');
    Route::get('/employee/delete/{id}', 'destroy')->name('employee.delete');
    Route::get('/employee/clone/{id}', 'clone')->name('employee.clone');
});
