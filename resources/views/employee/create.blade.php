@extends('layouts.app')

@section('content')
    <main>
        <div class="row row-cols-1 row-cols-md-3 mb-3">
            <div class="col-lg-12">
                <div class="pricing-header p-3 pb-md-4 d-flex justify-content-between">
                    <h3 class="fw-normal text-body-emphasis">Create Employee</h3>
                    <a href="{{ route('employee') }}" class="btn btn-secondary">Back</a>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="card mb-4 rounded-3 shadow-sm">
                    <div class="card-body">
                        <form action="{{ route('employee.store') }}" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-lg-6 mb-3">
                                    <label for="first_name" class="form-label">First Name</label>
                                    <input type="text" class="form-control" id="first_name" name="first_name">
                                </div>
                                <div class="col-lg-6 mb-3">
                                    <label for="last_name" class="form-label">Last Name</label>
                                    <input type="text" class="form-control" id="last_name" name="last_name">
                                </div>
                                <div class="col-lg-6 mb-3">
                                    <label for="date_of_birth" class="form-label">Date Of Birth</label>
                                    <input type="date" class="form-control" id="date_of_birth" name="date_of_birth">
                                </div>
                                <div class="col-lg-6 mb-3">
                                    <label for="gender" class="form-label">Gender</label>
                                    <select name="gender" id="gender" class="form-select">
                                        <option value="" selected disabled>Select Gender</option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                    </select>
                                </div>
                                <div class="col-lg-6 mb-3">
                                    <label for="age" class="form-label">Age</label>
                                    <input type="number" class="form-control" id="age" name="age" min="0">
                                </div>
                                <div class="col-lg-6 mb-3">
                                    <label for="salary" class="form-label">Salary</label>
                                    <input type="number" class="form-control" id="salary" name="salary" min="0">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
