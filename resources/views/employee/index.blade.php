@extends('layouts.app')

@section('content')
        <main>
            <div class="row row-cols-1 row-cols-md-3 mb-3">
                <div class="col-lg-12">
                    <div class="pricing-header p-3 pb-md-4 d-flex justify-content-between">
                        <h3 class="fw-normal text-body-emphasis">Laravel CRUD</h3>
                        <a href="{{ route('employee.create') }}" class="btn btn-primary">Add Employee</a>
                    </div>

                    @if(session('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('success') }}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                </div>

                <div class="col-lg-12">
                    <div class="card mb-4 rounded-3 shadow-sm">
                        <div class="card-body px-0">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Date Of Birth</th>
                                        <th>Gender</th>
                                        <th>Age</th>
                                        <th>Salary</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if($employees->count() > 0)
                                    @foreach($employees as $key => $value)
                                        <tr>
                                            <td>{{ ++$key }}</td>
                                            <td>{{ $value->first_name }}</td>
                                            <td>{{ $value->last_name }}</td>
                                            <td>{{ $value->date_of_birth }}</td>
                                            <td>{{ $value->gender }}</td>
                                            <td>{{ $value->age }}</td>
                                            <td>${{ $value->salary }}</td>
                                            <td>
                                                <a href="{{ route('employee.clone', $value->id) }}" class="btn btn-info btn-sm">Clone</a>
                                                <a href="{{ route('employee.edit', $value->id) }}" class="btn btn-warning btn-sm">Edit</a>
                                                <a href="{{ route('employee.delete', $value->id) }}" class="btn btn-danger btn-sm">Delete</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="8" class="text-center">No data is available in table</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                            <div class="px-3">
                                {{ $employees->links('vendor.pagination.bootstrap-5') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
@endsection
